require 'net/http'
require 'json'
require 'digest'
require 'mechanize'

module Temp
  module Mail
     class Client

      attr_accessor :email
      attr_accessor :key

      def initialize
        response = Net::HTTP.get_response(
            URI('https://post-shift.ru/api.php?action=new&type=json')
        )
        data = JSON.parse(response.body)
        p data
        self.email = data["email"]
        self.key = data["key"]
      end

      def delete_mail
        response = Net::HTTP.get_response(
            URI('https://post-shift.ru/api.php?action=delete&key=%{key}&type=json' %{ :key => self.key})
        )
        JSON.parse(response.body)
      end

      def get_incoming_emails
        response = Net::HTTP.get_response(
            URI('https://post-shift.ru/api.php?action=getlist&key=%{key}&type=json' %{ :key => self.key})
        )

        if response.is_a?(Net::HTTPNotFound)
          []
        else
          JSON.parse(response.body)
        end
      end

      def get_incoming_email(id)
        response = Net::HTTP.get_response(
            URI('https://post-shift.ru/api.php?action=getmail&key=%{key}&id=%{id}&type=json' %{ :key => self.key, :id => id})
        )
        JSON.parse(response.body)
      end

     end
  end

    module Account
      class Client

        attr_accessor :form

        def initialize
          agent = Mechanize.new
          agent.get('https://dev.by/registration')
          self.form = agent.page.forms[0]

        end

        def submit_form(username, email, password, user_agreement=true)
          self.form['user[username]'] = username
          self.form['user[email]'] = email
          self.form['user[password]'] = password
          self.form['user[password_confirmation]'] = password
          self.form['user_agreement'] = user_agreement
          self.form.submit

        end

      end

    end

end


users_numbers = ARGV[0]
logins = []

i = 0
while i <= users_numbers.to_i - 1
  mail_client = Temp::Mail::Client.new
  form_client = Temp::Account::Client.new

  form_client.submit_form(
      mail_client.email.split('@')[0],
      mail_client.email,
      mail_client.email.split('@')[0]
  )

  sleep 10

  message = mail_client.get_incoming_email(1)

  mail_client.delete_mail

  p confirm_url = message["message"].split('<')[1].tr('<>','')
  Net::HTTP.get_response(URI(confirm_url))

  logins = logins + [mail_client.email + ' ' + mail_client.email.split('@')[0]]

  i += 1
end

p logins